import { HttpClient, HttpParams } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { map, tap } from 'rxjs';
import { LivrosResultado } from '../models/interfaces';

@Injectable({
  providedIn: 'root'
})
export class LivroService {
  private readonly url = 'https://www.googleapis.com/books/v1/volumes';

  constructor(private readonly http: HttpClient) { }

  buscar(valorDigitado: string) {
    const params = new HttpParams().append('q', valorDigitado);
    return this.http.get<LivrosResultado>(this.url, { params });
  }
}
