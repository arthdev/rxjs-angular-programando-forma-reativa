import { Component, OnDestroy } from '@angular/core';
import { FormControl } from '@angular/forms';
import { catchError, debounceTime, EMPTY, filter, finalize, interval, map, of, Subscription, switchMap, take, tap, throwError } from 'rxjs';
import { Livro, LivrosResultado } from 'src/app/models/interfaces';
import { LivroVolumeInfo } from 'src/app/models/livroVolumeInfo';
import { LivroService } from 'src/app/service/livro.service';

@Component({
  selector: 'app-lista-livros',
  templateUrl: './lista-livros.component.html',
  styleUrls: ['./lista-livros.component.css']
})
export class ListaLivrosComponent {
  campoBusca = new FormControl('');
  subscription: Subscription;
  mensagemErro: string;
  livrosResultado: LivrosResultado = null;

  readonly livrosEncontados$ = this.campoBusca.valueChanges
    .pipe(
      tap((valorDigitado) => console.log('valor digitado.', valorDigitado)),
      // apenas se tiver digitado mais de 3 caracteres
      filter(valorDigitado => valorDigitado && valorDigitado.length > 3),
      tap((valorDigitado) => console.log('valor digitado passou pelo filtro.', valorDigitado)),
      // aguardar 500 milisegundos antes de prosseguir
      debounceTime(500),
      switchMap((valorDigitado) => this.service.buscar(valorDigitado)),
      tap((resultado) => { 
        console.log('requisicao feita.', resultado);
        this.livrosResultado = resultado;
      }),
      map(resultado => resultado.items ?? []),
      map(items => this.livrosResultadoParaLivros(items)),
      catchError(() => {
        this.mensagemErro = 'Ops, ocorreu um erro. Recarregue a aplicação.';
        return EMPTY;
      })
    );
  

  constructor(private readonly service: LivroService) {
  }

  livrosResultadoParaLivros(items) {
    return items.map(item => new LivroVolumeInfo(item));
  }
}



