export interface Livro {
    title?: string;
    authors?: string[];
    publiser?: string;
    publishedDate?: string;
    description?: string;
    previewLink?: string;
    thumbnail?: string;
}

export interface LivrosResultado {
    items: any[];
    totalItems: number;
}