export class LivroVolumeInfo {
    title?: string;
    authors?: string[];
    publiser?: string;
    publishedDate?: string;
    description?: string;
    previewLink?: string;
    thumbnail?: string;

    constructor(item) {
        this.title = item.volumeInfo?.title;
        this.authors = item.volumeInfo?.authors;
        this.publiser = item.volumeInfo?.publiser;
        this.publishedDate = item.volumeInfo?.publishedDate;
        this.description = item.volumeInfo?.description;
        this.previewLink = item.volumeInfo?.previewLink;
        this.thumbnail = item.volumeInfo?.imageLinks?.thumbnail;
    }
}